module.exports = {
	prompts() {
		return [
			{
				name: "name",
				message: "Projectname (repo/folder name)",
				default: this.outFolder,
				filter: (val) => val.toLowerCase(),
			},
			{
				name: "title",
				message: "Project title",
			},
			{
				name: "brandColor",
				message: "Brand color hash",
			},
			{
				name: "useAuth",
				message: "Add authentication (Nuxt Auth / JWT)?",
				type: 'list',
    			choices: [
					{ name: 'Yes', value: true },
					{ name: 'No', value: false }
				],
				default: true,
			},
		];
	},
	actions: [
		{
			type: "add",
			files: "**",
		},
		{
			type: "move",
			patterns: {
				gitignore: ".gitignore",
			},
		},
	],
	async completed() {
		this.gitInit();
		await this.npmInstall({ npmClient: 'yarn' });
		this.showProjectTips();
	},
};
