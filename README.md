# Nuxt Webapp project generator

## Usage

Install [SAO](https://github.com/saojs/sao) first.

```bash
yarn global add sao
# or
npm i -g sao
```

### From git

```bash
sao bitbucket:skeps/sao-skeps-nuxt-generator my-project
sao direct:git@bitbucket.org:skeps/sao-skeps-nuxt-generator.git my-project
```
