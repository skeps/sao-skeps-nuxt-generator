export class AuthService {
    constructor($axios) {
		this.$axios = $axios;
	}

	createAccount({ firstName, lastName, email, password, companyName }) {
		return this.$axios.$post('/api/auth/register', {
			first_name: firstName,
			last_name: lastName,
			email,
			password,
			company_name: companyName,
		});
	}

	requestPasswordReset(email) {
		return this.$axios.$post('/api/auth/request-password-reset', {
			email
		});
	}

	resetPassword({ email, resetToken, password, passwordConfirmation }) {
		return this.$axios.$post('/api/auth/reset-password', {
			email,
			token: resetToken,
			password,
			password_confirmation: passwordConfirmation,
		});
	}
}
