import Vue from 'vue';
import { ValidationProvider, ValidationObserver, extend } from 'vee-validate';
import { regex, required, email } from 'vee-validate/dist/rules';

extend('regex', {
    ...regex,
    message: 'Vul een geldige waarde in'
});

extend('required', {
    ...required,
    message: 'Dit veld is verplicht'
});

extend('email', {
    ...email,
    message: 'Vul een geldig e-mailadres in'
});

extend('min', {
    validate(value, args) {
        return value.length >= args.length;
    },

    message(args) {
		return `Veld moet minimaal ${args.length} tekens bevatten.`;
	},

    params: ['length'],
});

extend('password', {
    params: ['target'],

    validate(value, { target }) {
        return value === target;
    },

    message: 'Wachtwoorden komen niet overeen',
});

Vue.component('ValidationProvider', ValidationProvider);
Vue.component('ValidationObserver', ValidationObserver);
