import { AuthService } from '@/services/AuthService';

export default ({ $axios }, inject) => {
	inject('api', {
		auth: new AuthService($axios),
	});
};
