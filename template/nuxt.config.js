export default {
	ssr: false,

	head: {
		title: '<%= title %>',
		meta: [
			{ charset: 'utf-8' },
			{ name: 'viewport', content: 'width=device-width, initial-scale=1, user-scalable=no, maximum-scale=1', },
			{ hid: 'description', name: 'description', content: '' },
			{ name: 'format-detection', content: 'telephone=no' },
		],
		link: [
			{ rel: 'icon', type: 'image/png', href: '/icon.png' },
			// { rel: 'stylesheet', href: 'https://use.typekit.net/rzp6mpl.css' },
		],
	},

    components: [
		{ path: '~/components/' },
		{ path: '~/components/Global', prefix: 'Global' },
		{ path: '~/components/Interface', prefix: 'Interface' },
		{ path: '~/components/Form', prefix: 'Form' },
	],

	css: [],

	plugins: [
		'~/plugins/api.plugin',
		'~/plugins/vee-validate',
		'~/plugins/pwa-update',
	],

    loading: {
		height: '4px',
		color: '<%= brandColor %>',
	},

	buildModules: [,
        '@nuxtjs/tailwindcss',
		'@nuxtjs/svg-sprite',
    ],

	modules: [
        '@nuxtjs/axios',
        '@nuxtjs/pwa',
        '@nuxtjs/auth-next',
		'portal-vue/nuxt',
    ],

	axios: {
		baseURL: process.env.API_BASE_URL || 'http://local-backend.test',
	},

	tailwindcss: {
		cssPath: '@/assets/styles/main.scss',
		configPath: './tailwind.config.js'
	},

	pwa: {
        manifest: {
			lang: 'nl',
            name: '<%= title %>',
			short_name: '<%= title %>',
			theme_color: '<%= brandColor %>',
        }
	},

	router: {
		// middleware: ['auth'],
	},

    auth: {
		strategies: {
			'laravelJWT': {
				provider: 'laravel/jwt',
				url: process.env.API_BASE_URL || 'http://neyfik-audittool.test',
				endpoints: {
					login: { url: '/api/auth/login', method: 'post' },
					logout: { url: '/api/auth/logout', method: 'post' },
					refresh: { url: '/api/auth/refresh', method: 'post' },
					user: { url: '/api/auth/user', method: 'get' },
				},
				token: {
					property: 'access_token',
					maxAge: 10080
				},
				refreshToken: {
					maxAge: 20160 * 60
				},
				user: {
					property: 'data',
				},
			},
		},

		redirect: {
			home: '/',
			login: '/inloggen',
			logout: '/',
			callback: '/inloggen',
		},
	},
};
