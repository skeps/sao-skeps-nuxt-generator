const plugin = require('tailwindcss/plugin');

module.exports = {
	theme: {
		colors: {
			primary: '<%= brandColor %>',
			white: '#ffffff',
			transparent: 'transparent',

			danger: '#EB5757',
			warning: '#F2994A',
			success: '#27AE60',

			gray: {
				50: '#F2F2F2',
				100: '#E0E0E0',
				200: '#BDBDBD',
				300: '#828282',
				400: '#4F4F4F',
				500: '#333333',
				700: '#222222',
				900: '#111111',
			},
		},

		fontFamily: {
			'heading': [
				'Inter',
				'-apple-system',
				'BlinkMacSystemFont',
				'"Segoe UI"',
				'"Helvetica Neue"',
				'Arial',
				'sans-serif'
			],
			'body': [
				'Inter',
				'-apple-system',
				'BlinkMacSystemFont',
				'"Segoe UI"',
				'"Helvetica Neue"',
				'Arial',
				'sans-serif'
			]
		},

		extend: {
			boxShadow: {
				default: '0px 2px 80px 20px rgba(0, 0, 0, 0.08)',
				card: '0px 0px 32px rgba(0, 0, 0, 0.04)',
			}
		}
	},

	corePlugins: {
		container: false,
	},

	purge: {
        content: [
            './components/**/*.vue',
            './pages/**/*.vue',
            './layouts/**/*.vue'
        ]
    }
}
