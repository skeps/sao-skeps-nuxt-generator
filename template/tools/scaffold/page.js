module.exports = {
	option: 'Page',
	defaultCase: '(pascalCase)',
	entry: {
		folderPath: './tools/templates/page',
	},
	stringReplacers: [
		{ question: 'Enter page filename', slot: '__filename__' },
		{ question: 'Enter page identifier name', slot: '__pageName__' },
	],
	output: {
		path: './pages/',
		pathAndFileNameDefaultCase: '(snakeCase)',
		overwrite: true,
	},
	onComplete: res => console.log('Page generated'),
};
