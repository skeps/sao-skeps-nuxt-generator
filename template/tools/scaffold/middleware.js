module.exports = {
	option: 'Middleware',
	defaultCase: '(pascalCase)',
	entry: {
		folderPath: './tools/templates/middleware',
	},
	stringReplacers: [
		{ question: 'Enter middleware name', slot: '__filename__' },
	],
	output: {
		path: './middleware/',
		pathAndFileNameDefaultCase: '(pascalCase)',
		overwrite: true,
	},
	onComplete: res => console.log('Middleware generated'),
};
