module.exports = {
	option: 'Component',
	defaultCase: '(pascalCase)',
	entry: {
		folderPath: './tools/templates/component',
	},
	stringReplacers: [
		{ question: 'Enter component filename', slot: '__filename__' },
		{ question: 'Enter component identifier name', slot: '__componentName__' },
	],
	output: {
		path: './components/',
		pathAndFileNameDefaultCase: '(pascalCase)',
		overwrite: true,
	},
	onComplete: res => console.log('Component generated'),
};
