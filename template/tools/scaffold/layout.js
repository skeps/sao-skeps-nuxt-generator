module.exports = {
	option: 'Layout',
	defaultCase: '(pascalCase)',
	entry: {
		folderPath: './tools/templates/layout',
	},
	stringReplacers: [
		{ question: 'Enter layout filename', slot: '__filename__' },
		{ question: 'Enter layout identifier name', slot: '__layoutName__' },
	],
	output: {
		path: './layouts/',
		pathAndFileNameDefaultCase: '(kebabCase)',
		overwrite: true,
	},
	onComplete: res => console.log('Layout generated'),
};
