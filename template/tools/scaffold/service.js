module.exports = {
	option: 'Service',
	defaultCase: '(pascalCase)',
	entry: {
		folderPath: './tools/templates/service',
	},
	stringReplacers: [
		{ question: 'Enter service name', slot: '__name__' },
	],
	output: {
		path: './services/',
		pathAndFileNameDefaultCase: '(pascalCase)',
		overwrite: true,
	},
	onComplete: res => console.log('Service generated'),
};
