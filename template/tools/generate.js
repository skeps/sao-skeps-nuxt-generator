const { generateTemplateFiles } = require('generate-template-files');

generateTemplateFiles([
    require('./scaffold/component'),
    require('./scaffold/layout'),
    require('./scaffold/page'),
    require('./scaffold/middleware'),
    require('./scaffold/service'),
]);
